//
//  Array+MMExtensions.swift
//  MMUtils
//
//  Created by Trevor Lee on 2020/1/8.
//

import Foundation

public
extension Array where Element: Equatable {
    mutating func remove(_ object: Element) {
        if let index = firstIndex(of: object) {
            remove(at: index)
        }
    }
}
