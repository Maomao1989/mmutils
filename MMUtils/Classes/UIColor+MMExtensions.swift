//
//  UIColor+MMExtensions.swift
//  MMUtils
//
//  Created by Trevor Lee on 2020/1/8.
//

import Foundation

public
extension UIColor {
    convenience init(hex: UInt16, alpha: CGFloat = 1) {
        let divisor = CGFloat(15)
        let red     = CGFloat((hex & 0xF00) >> 8) / divisor
        let green   = CGFloat((hex & 0x0F0) >> 4) / divisor
        let blue    = CGFloat( hex & 0x00F      ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
