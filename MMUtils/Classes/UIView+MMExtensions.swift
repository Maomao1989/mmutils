//
//  UIView+MMExtensions.swift
//  MMUtils
//
//  Created by Trevor Lee on 2020/1/8.
//

import Foundation

public
extension UIView {
    var origin: CGPoint {
        get { self.frame.origin }
        set { self.frame = CGRect(origin: newValue, size: size) }
    }
    func setOrigin(x: CGFloat, y: CGFloat) {
        self.origin = CGPoint(x: x, y: y)
    }
    
    var size: CGSize {
        get { self.frame.size }
        set { self.frame = CGRect(origin: origin, size: newValue) }
    }
    func setSize(width: CGFloat, height: CGFloat) {
        self.size = CGSize(width: width, height: height)
    }
    
    var x: CGFloat {
        get { origin.x }
        set { setOrigin(x: newValue, y: y) }
    }
    
    var y: CGFloat {
        get { origin.y }
        set { setOrigin(x: x, y: newValue) }
    }
    
    var width: CGFloat {
        get { size.width }
        set { setSize(width: newValue, height: height) }
    }
    
    var height: CGFloat {
        get { size.height }
        set { setSize(width: width, height: newValue) }
    }
    
    var right: CGFloat {
        get { x + width }
        set { x = newValue - width }
    }
    
    var bottom: CGFloat {
        get { y + height }
        set { y = newValue - height }
    }
    
    var centerX: CGFloat {
        get { self.center.x }
        set { self.center = CGPoint(x: newValue, y: centerY) }
    }
    
    var centerY: CGFloat {
        get { self.center.y }
        set { self.center = CGPoint(x: centerX, y: newValue) }
    }
    
    var boundsCenter: CGPoint {
        get { CGPoint(x: width/2, y: height/2) }
    }
}
